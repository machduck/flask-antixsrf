#!/usr/bin/env python

"""
BEERWARE LICENSE (Revision 43):

<machduck@yandex.ru> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
"""

from flask import session, request, abort

from uuid import uuid4 as uuid

from functools import wraps
from types import MethodType

class Strategy( object ):
	"""
	Strategy
	"""
	def strategy( self, fn, strategy ):
		object.__setattr__( self, fn, MethodType( strategy, self ))

class XSRFHandler( Strategy ):
	"""
	app : Flask
		Flask instance to handle XSRF for.
		Only needed if jinja2 token injections are going to be used.

	inject : bool
		To inject tokens into jinja2 templates or not to inject. That is 
		the question, programmer.
		
	callback : function
		Function to call if token verification is successfull.

	errorback : function
		Function to call if token verification fails.

	gen_token : function
		Function to generate the xsrf tokens if you're worried about
		uuid.uuid4 entropy.

	COOKIE_NAME : string
		The name of the cookie containing the token.

	HEADER_NAME : string
		The name of the HTTP header containing the token.

	TOKEN_KEY : string
		The GET/POST parameter expected to contain the token.

	Example:
		xsrfh = XSRFHandler( app )

		@app.route( '/token' )
		@xsrfh.send()
		def token():
			return make_response(render_template( 'xsrf.html' ))

		@app.route( '/xsrf', methods = [ 'GET', 'POST' ] )
		@xsrfh.verify()
		def xsrf():
			return make_response( 'XSRF' )
	"""

	ALL    = 0x1
	FORM   = 0x2 # send only via form
	HEADER = 0x3 # send only via header

	COOKIE_NAME = 'uuid'   # xsrf cookie name
	HEADER_NAME = 'X-XSRF' # xsrf header name
	TOKEN_KEY   = 'token'  # xsrf token  key

	def handle_all( self ):
		"""
		Handles all types of XSRF checks via delegating to implementations
		"""
		if( not(  self.check_header() or self.check_form() )):
			self.errorback()
		self.callback()

	def handle_header( self ):
		"""
		Handles header type XSRF checks
		"""
		if( not self.check_header() ):
			self.errorback()
		self.callback()

	def handle_form( self ):
		"""
		Handles form type XSRF checks
		"""
		if( not self.check_form() ):
			self.errorback()
		self.callback()

	def check_header( self ):
		"""
		Check for xsrf token in header
		Optionally nulls token to stop csrf replay fuzzing

		Returns True if tokens matched
		"""

		if( self._header not in request.headers.keys() ):
			return
		if( session[ self._cookie ] != request.headers[ self._token ] ):
			return

		if( self.reset ):
			session[ self._cookie ] = None

		return True

	def check_form( self ):
		"""
		Check for xsrf token in parameters
		Optionally nulls token to stop csrf replay fuzzing

		Returns True if tokens matched
		"""

		if( self._token not in request.values.keys() ):
			return
		if( session[ self._cookie ] != request.values[ self._token ] ):
			return

		if( self.reset ):
			session[ self._cookie ] = None

		return True

	def xsrf_token( self ):
		"""
		Generates xsrf token and saves it

		Returns xsrf token
		"""
		token = self.gen_token()
		session[ self._cookie ] = token
		return token

	def verify( self, type = ALL ):
		"""
		Verify anti-xsrf token
		"""
		def decorator( func ):
			@wraps( func )
			def wrapper( *args, **kwargs ):
				request = func( *args, **kwargs )

				if( type == self.ALL ):
					self.handle_all()
				elif( type == self.FORM ):
					self.handle_form()
				elif( type == self.HEADER ):
					self.handle_header()
				else:
					raise TypeError( type )

				return request 
			return wrapper
		return decorator

	def send( self, type = ALL ):
		"""
		Generate anti-xsrf token
		"""
		def decorator( func ):
			@wraps( func )
			def wrapper( *args, **kwargs ):

				token = self.xsrf_token()
				if( self.inject ):
					self.app.jinja_env.globals \
							.update( xsrf_token = token )
				response = func( *args, **kwargs )
				if( type == self.ALL or type == self.HEADER ):
					response.headers.add( self._header, token )

				return response
			return wrapper
		return decorator

	def _callback( self ):
		"""
		Called when token is verified
		"""
		pass

	def _errorback( self ):
		"""
		Called when token is not verified
		"""
		abort( 403 )

	def _gen_token( self ):
		"""
		Generate token
		"""
		return uuid().hex

	def __init__( 	self,
					app = None,
					reset = True,
					inject = True,
					callback = None,
					errorback = None,
					gen_token = None ):

		if( app is not None ): 
			# app is only needed to inject xsrf related stuff into templates
			self.app = app
			self.inject = inject
		else:
			self.inject = False

		self.reset = reset

		if( callback is None ):
			callback = XSRFHandler._callback
		self.strategy( 'callback', callback )

		if( errorback is None ):
			errorback = XSRFHandler._errorback
		self.strategy( 'errorback', errorback )

		if( gen_token is None ):
			gen_token = XSRFHandler._gen_token
		self.strategy( 'gen_token', gen_token )

		self._header = XSRFHandler.HEADER_NAME
		self._token = XSRFHandler.TOKEN_KEY
		self._cookie = XSRFHandler.COOKIE_NAME
