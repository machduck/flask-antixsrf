#!/usr/bin/env python

from flask import Flask, render_template, make_response

app = Flask( __name__ )
app.secret_key = "1234567890abcdef"

from xsrf import XSRFHandler
xsrfh = XSRFHandler( app )

"""
Basic example
"""

@app.route( '/token' )
@xsrfh.send()
def token():
	return make_response(render_template( 'xsrf.html' ))

@app.route( '/xsrf', methods = [ 'POST', 'GET' ] )
@xsrfh.verify()
def xsrf():
	return make_response( 'XSRF' )

"""
Header example
"""

@app.route( '/header_token' )
@xsrfh.send( XSRFHandler.HEADER )
def header_token():
	return make_response( 'Token sent' )

@app.route( '/xsrf', methods = [ 'POST', 'GET' ] )
@xsrfh.verify( XSRFHandler.HEADER )
def xsrf_header():
	return make_response( 'XSRF header' )

"""
Form example
"""

@app.route( '/form_token' )
@xsrfh.send( XSRFHandler.FORM )
def form_token():
	return make_response(render_template( 'xsrf.html' )) 

@app.route( '/xsrf', methods = [ 'POST', 'GET' ] )
@xsrfh.verify( XSRFHandler.FORM )
def xsrf_form():
	return Response( 'XSRF form' )

if __name__ == "__main__":
	app.run( debug = True )
