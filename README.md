# Flask Anti-XSRF

Flask's default session is kept **clientside in a signed cookie**.

You have been warned.

# Just another anti-XSRF token library..

Only *slightly* better:

* View-oriented control instead of by access method type (Flask-SeaSurf)
* Token is injected into jinja2 templates automatically (opt-out)
* Each token has one-time usage (opt-out), however scanners will be able to replay xsrf tokens via sending the same token and session (in a cookie) against which the token is verified.
* Less processor cycles because there is no need to encrypt the token in the cookie (Flask-xsrf)
* Can create multiple xsrf token handlers allowing for better flexibility
* Token delivery method control - header or page form or both
* (imho) user-friendly API
* Strategy pattern allowing custom callback / errorback to be injected into handler class

That's all folks!
